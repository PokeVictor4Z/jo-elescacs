package ElExercisi;

import java.util.ArrayList;
import java.util.Scanner;

public class Exdercisi {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int h = sc.nextInt();
		sc.nextLine();
		ArrayList<String> moves = new ArrayList<String>();
		ArrayList<Integer> com = new ArrayList<Integer>();

		for (int i = 0; i < h; i++) {

			moves.add(sc.next());

		}
		com = soluciones(moves);
		

	}

	private static boolean esticFora(int[][] mat, int f, int c) {
		if (f < 0 || c < 0 || f > mat.length - 1 || c > mat[0].length - 1) {
			return true;
		} else {
			return false;
		}

	}

	public static ArrayList<Integer> soluciones(ArrayList<String> moves) {

		int[][] mat = new int[8][8];
		ArrayList<Integer> com = new ArrayList<Integer>();
		for (int xy = 0; xy < moves.size(); xy++) {
			String[] pos = moves.get(xy).split("");
			String peça = pos[0].toLowerCase();
			String columnes = pos[1];
			int files = Integer.parseInt(pos[2]);
			files -= 1;
			int columna = 0;
			int comptador = 0;

			switch (columnes) {

			case "a":
				columna = 0;
				break;
			case "b":
				columna = 1;
				break;
			case "c":
				columna = 2;
				break;
			case "d":
				columna = 3;
				break;
			case "e":
				columna = 4;
				break;
			case "f":
				columna = 5;
				break;
			case "g":
				columna = 6;
				break;
			case "h":
				columna = 7;
				break;

			}

			if (peça.equals("a")) {

				for (int z = 1; z < mat.length; z++) {

					if (esticFora(mat, files + z, columna + z) == false) {
						comptador++;
					}

				}
				for (int y = 1; y < mat.length; y++) {

					if (esticFora(mat, files - y, columna + y) == false) {
						comptador++;
					}

				}
				for (int w = 1; w < mat.length; w++) {

					if (esticFora(mat, files - w, columna - w) == false) {
						comptador++;
					}

				}
				for (int f = 1; f < mat.length; f++) {

					if (esticFora(mat, files + f, columna - f) == false) {
						comptador++;
					}

				}
			}
			if (peça.equals("t")) {

				for (int z = 1; z < mat.length; z++) {

					if (esticFora(mat, files + z, columna) == false) {
						comptador++;
					}

				}
				for (int y = 1; y < mat.length; y++) {

					if (esticFora(mat, files - y, columna) == false) {
						comptador++;
					}

				}
				for (int w = 1; w < mat.length; w++) {

					if (esticFora(mat, files, columna + w) == false) {
						comptador++;
					}

				}
				for (int f = 1; f < mat.length; f++) {

					if (esticFora(mat, files, columna - f) == false) {
						comptador++;
					}

				}
				
				
			}
			System.out.println(comptador);
			com.add(comptador);
			
		}
		
		return com; 
	}
}
